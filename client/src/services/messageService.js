import { callApi } from '../helpers/apiHelper';

class MessageService {
    getAllMessages = async (messagesURL) => {
        const messages = await callApi(messagesURL);
        
        const sortedMessagesList = messages.slice().sort((a, b) => {
            return new Date(a.createdAt) - new Date(b.createdAt);
        });

        return sortedMessagesList;
    }
}
export const messageService = new MessageService();