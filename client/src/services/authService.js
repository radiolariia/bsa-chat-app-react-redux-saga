import { callApi } from '../helpers/apiHelper';

class AuthService {
    login = async (URL, request) => {
    const response = await callApi(URL, {
        method: 'POST',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
        },
        body: JSON.stringify({...request})
    });

    return response.json();
};
}

export const authService = new AuthService();