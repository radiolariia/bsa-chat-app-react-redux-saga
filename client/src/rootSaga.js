import { fetchMessagesWorker } from './components/Chat/sagas/sagas';
import { loginWorker } from './components/LoginPage/sagas/sagas';
import { all } from 'redux-saga/effects';

export function* rootSaga() {
    yield all([
        // loginWorker(),
        fetchMessagesWorker(),
    ])
}