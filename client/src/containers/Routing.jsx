import React from 'react';
import { Switch, Redirect } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PublicRoute from '../components/PublicRoute/index';
import PrivateRoute from '../components/PrivateRoute/index';
import LoadingScreen from '../components/LoadingScreen/LoadingScreen';
import LoginPage from '../components/LoginPage/LoginPage';
import Chat from '../components/Chat/Chat';
import EditPage from '../components/EditPage/EditPage';
import UserList from '../components/UserList/UserList';

const Routing = ({
//   user,
//   isAuthorized,
//   applyPost: newPost,
//   logout: signOut,
//   loadCurrentUser: loadUser,
  isLoading
}) => {
//   useEffect(() => {
//     if (!isAuthorized) {
//       loadUser();
//     }
//   });

  return (
    isLoading
      ? <LoadingScreen />
      : (
        <div className="fill">
          {/* {isAuthorized && (
            <header>
              <Header user={user} logout={signOut} />
            </header>
          )} */}
          <main className="fill">
            <Switch>
              <PublicRoute exact path="/login" component={LoginPage} />
              <PublicRoute exact path="/chat" component={Chat} />
              <PublicRoute exact path="/editor/message" component={EditPage} />
              <PublicRoute exact path="/editor/user" component={EditPage} />
              <PublicRoute exact path="/users" component={UserList} />
              <Redirect to="/chat"/>
            </Switch>
          </main>
        </div>
      )
  );
};

// const actions = { loadCurrentUser, logout, applyPost };

// const mapStateToProps = ({ profile }) => ({
//   isAuthorized: profile.isAuthorized,
//   user: profile.user,
//   isLoading: profile.isLoading
// });

export default Routing;
