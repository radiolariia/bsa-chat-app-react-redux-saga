import React from 'react';
import './DayLabel.css';

function DayLabel({ createdAt }) {
    const formatter = new Intl.DateTimeFormat('en', { month: 'long' });
    const creationTime = new Date(createdAt);
    const creationMonth = formatter.format(creationTime);
    const creationDay = creationTime.getDate();
    const today = new Date().getDate();
    const labelText =  today - creationDay === 1 ? 'Yesterday' : today === creationDay ? 'Today' : creationDay + ' ' + creationMonth;

    return (
        <div className="day-label-container">
            <div className="day-label">{labelText}</div>
        </div>
    );
}

export default DayLabel;