import React, { useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import './LoginPage.css';
import validator from 'validator';
import ErrorModal from '../ErrorModal/ErrorModal';
import {
  login,
  hideError 
} from './actions';

const LoginForm = ({ login, isLoading, errorMessage, hideError }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [isUsernameValid, setIsUsernameValid] = useState(true);
  const [isPasswordValid, setIsPasswordValid] = useState(true);

  const usernameChanged = data => {
    setUsername(data);
    setIsUsernameValid(true);
  };

  const passwordChanged = data => {
    setPassword(data);
    setIsPasswordValid(true);
  };

  const handleLoginClick = async () => {
    const isValid = isUsernameValid && isPasswordValid;
    console.log(!isValid || isLoading)
    if (!isValid || isLoading) {
      return;
    }
    
    await login({ username, password });
  };

  return (
    <div className="fill">
        {errorMessage !== '' ? <ErrorModal errorMessage={errorMessage} hideError={hideError}/> : null}
        <div id="login-window">
        <span>Log in</span>
            <div id="login-input__container">
                <form id="login-form" autoComplete="off" name="loginForm">
                    <input
                        id="login-input__email"
                        autoComplete="off" 
                        placeholder="Username" 
                        type="text"
                        onChange={ev => usernameChanged(ev.target.value)}
                        onBlur={() => setIsUsernameValid(Boolean(username) && !validator.contains(username, '@'))}
                    />
                    <input
                        id="login-input__password"
                        autoComplete="off" 
                        placeholder="Password"
                        type="password"
                        onChange={ev => passwordChanged(ev.target.value)}
                        onBlur={() => setIsPasswordValid(Boolean(password))}
                    />
                </form>
            </div>
            <button className="login__button" type="submit" onSubmit={() => handleLoginClick()}>Login</button>
        </div>
    </div>
  );
};

const mapStateToProps = state => ({
  user: state.loginReducer.user,
  isAuthorized: state.loginReducer.isAuthorized,
  isLoading: state.loginReducer.isLoading,
  errorMessage: state.loginReducer.errorMessage
});

const actions = {
  login,
  hideError
}

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);