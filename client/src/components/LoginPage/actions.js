import { 
  LOGIN, 
  LOGIN_SUCCESS, 
  LOGOUT,
  HIDE_ERROR 
} from './actionTypes';

export const login = request => ({
  type: LOGIN,
  payload: {
      request
  }
});

export const loginSuccess = user  => ({
    type: LOGIN_SUCCESS,
    payload: {
        user
    }
  });

export const logout = () => ({
    type: LOGOUT,
});

export const hideError = () => {
  return ({
  type: HIDE_ERROR,
})};