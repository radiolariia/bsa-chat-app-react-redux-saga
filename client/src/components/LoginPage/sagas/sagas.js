import { put, call, takeLeading } from 'redux-saga/effects';
import { LOGIN, LOGIN_SUCCESS, LOGOUT } from '../actionTypes';
import { authService } from '../../../services/authService';
import { loginSuccess } from '../actions';

export function* loginWorker(action) {
    console.log(action)
    try {
        const user = yield call(authService.login, '/login');
        yield put({ type: LOGIN_SUCCESS, payload: { user } })
    } catch (error) {
        
        yield put({ 
            type: LOGOUT, 
            payload: { 
                errorMessage: 'loading messages error: ' + error.message 
            } 
        });
    }
}

export function* watchFetch() {
    yield takeLeading(LOGIN, loginWorker);
}