import { 
    LOGIN, 
    LOGIN_SUCCESS, 
    LOGOUT,
    HIDE_ERROR 
  } from './actionTypes';

const initialState = {
    user: {},
    isAuthorized: false,
    isLoading: false,
    errorMessage: ''
}

export const loginReducer = (state = initialState, action) => {
  switch(action.type) {
    case LOGIN:
      return {
        ...state,
        isLoading: true
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        user: action.user,
        isAuthorized: true,
        isLoading: false
      };
    case LOGOUT:
      return {
        ...state,
        user: {},
        isAuthorized: false,
        isLoading: false,
        errorMessage: action.payload.errorMessage
      };
    case HIDE_ERROR:
        return {
            ...state,
            isLoading: false,
            errorMessage: '',
        }
    default:
      return state;
  }
};
