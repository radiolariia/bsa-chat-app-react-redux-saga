import React, { useEffect } from 'react';
import './ErrorModal.css';

const ErrorModal = ({
  errorMessage,
  hideError
}) => {

    useEffect(() => {
        onHideError();
    })
  
    const onHideError = () => {
        setTimeout(hideError, 10000);
    }

    return (
        <div id="error-modal">
            {errorMessage}
        </div>
    );
}

export default ErrorModal;