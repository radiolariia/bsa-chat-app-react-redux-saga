import { put, call, takeLatest } from 'redux-saga/effects';
import {
    FETCH_MESSAGES, 
    FETCH_MESSAGES_SUCCESS, 
    FETCH_MESSAGES_FAILED
} from '../actionTypes';
import { messageService } from '../../../services/messageService';
import { API_URL } from '../../../config';

export function* fetchMessagesWorker() {
    try {
        const messages = yield call(messageService.getAllMessages, API_URL);
        yield put({ type: FETCH_MESSAGES_SUCCESS, payload: { messages } });
    } catch (error) {
        yield console.log(error.message);
        yield put({ 
            type: FETCH_MESSAGES_FAILED, 
            payload: { 
                error: 'loading messages error: ' + error.message 
            } 
        });
    }
}

export function* watchFetch() {
    yield takeLatest(FETCH_MESSAGES, fetchMessagesWorker);
}