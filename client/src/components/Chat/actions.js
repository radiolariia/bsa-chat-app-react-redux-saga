import {
    SEND_MESSAGE,
    FETCH_MESSAGES,
    FETCH_MESSAGES_SUCCESS,
    DELETE_MESSAGE,
    EDIT_MESSAGE,
    CANCEL_EDIT_MESSAGE,
    HIDE_ERROR
} from './actionTypes';

export const sendMessage = message => ({
    type: SEND_MESSAGE,
    payload: {
        message
    }
});

// bug with delete action is caused by incorrect API data 
//- all messages from a particular user have the same id what is usually prohibited
export const deleteMessage = id => ({
    type: DELETE_MESSAGE,
    payload: {
        id
    }
});

export const editMessage = (id, message) => ({
    type: EDIT_MESSAGE,
    payload: {
        id,
        message
    }
});

export const cancelEditMessage = () => ({
    type: CANCEL_EDIT_MESSAGE
});

// asynchronous actions

export const getMessages = () => ({
    type: FETCH_MESSAGES,
});

export const setMessages = (messages) => {
    return ({
        type: FETCH_MESSAGES_SUCCESS,
        payload: {
            messages
        }
})};

export const hideError = () => {
    return ({
    type: HIDE_ERROR,
})};

