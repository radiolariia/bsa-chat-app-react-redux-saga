import {
    SEND_MESSAGE, 
    DELETE_MESSAGE,
    EDIT_MESSAGE,
    CANCEL_EDIT_MESSAGE,
    HIDE_ERROR,
    FETCH_MESSAGES_SUCCESS,
    FETCH_MESSAGES_FAILED,
    FETCH_MESSAGES
} from './actionTypes';

const initialState = {
    isLoading: true,
    messages: [],
    errorMessage: ''
}

export const chatReducer = (state = initialState, action) => {
    switch(action.type) {
        case SEND_MESSAGE:
            const { id, message } = action.payload;
            const newMessage = { id, ...message };

            return {
                ...state,
                messages: [...(state.messages || []), newMessage],
            }
        case FETCH_MESSAGES:
            return {
                ...state,
                isLoading: true,
            }
        case FETCH_MESSAGES_SUCCESS:
            console.log(action)
            const { messages } = action.payload;
            return {
                ...state,
                isLoading: false,
                messages: [...(state.messages || []), ...messages]
            }
        case FETCH_MESSAGES_FAILED:
            const { error } = action.payload;
            console.log(error)
            return {
                ...state,
                isLoading: false,
                errorMessage: error,
            }

        // bug with delete action is caused by incorrect API data 
        //- all messages from a particular user have the same id what is usually prohibited
        case DELETE_MESSAGE:
            const filteredMessages = state.messages.slice().filter(msg => msg.id !== action.payload.id);

            return {
                ...state,
                messages: [...filteredMessages],
            }
        case EDIT_MESSAGE:
            const updatedMessages = state.messages.map(msg => {
                if(msg.id === action.payload.id) {
                    return {
                        ...msg,
                        ...action.payload.message
                    }
                } else {
                    return msg
                }
            })
            return {
                ...state,
                messages: updatedMessages,
            }
        case CANCEL_EDIT_MESSAGE:
            return {
                ...state,
                editingMessageText: ''
            }
        case HIDE_ERROR:
            return {
                ...state,
                isLoading: false,
                errorMessage: '',
            }
        default:
            return state;
    }
}