import React, { useState, useEffect } from 'react';
import './Chat.css';
import LoadingScreen from '../LoadingScreen/LoadingScreen.jsx';
import Header from '../Header/Header';
import MessageList from '../MessageList/MessageList';
import MessageInput from '../MessageInput/MessageInput';
import ErrorModal from '../ErrorModal/ErrorModal';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  sendMessage,
  getMessages,
  deleteMessage,
  editMessage,
  hideError
} from './actions';

const Chat = ({
  messages = [],
  sendMessage,
  getMessages,
  deleteMessage,
  editMessage,
  cancelEditMessage,
  hideError,
  isLoading,
  errorMessage
}) => {
    // eslint-disable-next-line
    const [activeUser, setActiveUser] = useState('Ruth');
    // eslint-disable-next-line
    const [activeUserAvatar, setActiveUserAvatar] = useState('https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA');
    
    useEffect(() => {
        getMessages();
    }, [getMessages]);
    
    const onAddMessage = (text) => {
      const newMessage = {
        "text": text, 
        "user": activeUser, 
        "avatar": activeUserAvatar, 
        "editedAt": "", 
        "createdAt": new Date()
      };

      sendMessage(newMessage);
    }

    // bug with delete action is caused by incorrect API data 
    //- all messages from a particular user have the same id what is usually prohibited
    const onDeleteMessage = (id) => {
      deleteMessage(id);
    }

    const participantsSet = new Set(messages.map(message => message.user));

  return (
      <React.Fragment>
          {errorMessage !== '' ? <ErrorModal 
            errorMessage={errorMessage} 
            hideError={hideError}/> 
          : null}
          {isLoading ? <LoadingScreen/>
          :
          <React.Fragment>
            <img id="logo" src={require("../../assets/logo.svg")} alt="paper plane logo"/>
            <div className="main-content">
              <Header
                chatName="My chat"
                numOfParticipants={participantsSet.size}
                numOfMessages={messages.length}
                lastMessageTime={14.28}
              />
              <MessageList 
                messages={messages} 
                activeUser={activeUser} 
                onDeleteMessage={onDeleteMessage}
                onEditMessage={editMessage}
                onCancelEdition={cancelEditMessage}
              />
              <MessageInput onAddMessage={onAddMessage}/>
            </div>
          </React.Fragment>
          }
      </React.Fragment>
  );
}

const mapStateToProps = state => ({
  messages: state.chatReducer.messages,
  isLoading: state.chatReducer.isLoading,
  errorMessage: state.chatReducer.errorMessage,
});

const actions = {
  sendMessage,
  getMessages,
  deleteMessage,
  editMessage,
  hideError,
}

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Chat);