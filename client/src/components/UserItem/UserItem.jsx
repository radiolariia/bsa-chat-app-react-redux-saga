import React from 'react';
import './UserItem.css';

const UserItem = ({
    user,
    email
}) => {
    return (
        <div className="user-item">
            <div className="user-item__text">
                <span className="user-item__name">{user}</span>
                <span className="user-item__mail">{email}</span>
            </div>
            <div className="user-item__buttons">
                <button className="user-item__edit btn">Edit</button>
                <button className="user-item__delete btn">Delete</button>
            </div>
        </div>
    );
}

export default UserItem;