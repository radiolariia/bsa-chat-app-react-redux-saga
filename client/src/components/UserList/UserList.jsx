import React, { useState, useEffect } from 'react';
import './UserList.css';
import UserItem from '../UserItem/UserItem';
import LoadingScreen from '../LoadingScreen/LoadingScreen.jsx';

const UserList = ({
    userList
}) => {
    return (
        <div className="user-list__page">
            <div className="user-list__container">
                <button className="user-list__button btn">Add user</button>
                {userList.forEach(user => {
                    return <UserItem user={user}/>;
                })}
            </div>
        </div>
    );
}

export default UserList;