import React, { useState, useRef } from 'react';
import './MessageInput.css';
import { Link } from 'react-router-dom';

function MessageInput({
  onAddMessage
}) {
    // eslint-disable-next-line
  const [typedMessage, setTypedMessage] = useState('');

  const writeMessage = (text) => {
    setTypedMessage(text);
  }

  const addMessage = (text) => {
    onAddMessage(text);
    textareaElem.current.value = '';
  }

  const textareaElem = useRef();
  return (
      <div id="message-input__container">
        <textarea 
          ref={textareaElem}
          id="message-input" 
          placeholder="Enter message" 
          name="Message" 
          cols="40" 
          rows="5"
          onChange={() => writeMessage(textareaElem.current.value)}
          ></textarea>
        <button 
          id="message-input__send"
          type="submit" 
          onClick={() => addMessage(typedMessage)}>
        <Link to='/editor/message'>Send</Link></button>
      </div>
  );
}

export default MessageInput;