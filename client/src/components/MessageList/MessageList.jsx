import React from 'react';
import './MessageList.css';
import Message from '../Message/Message';
import DayLabel from '../DayLabel/DayLabel';

const MessageList = ({
  messages, 
  activeUser, 
  onDeleteMessage, 
  showModal,
  hideModal,
  onEditMessage,
  onCancelEdition,
  isModalOpen
}) => {
  let messageCreationDate = new Date(messages[0].createdAt);

  return (
      <div id="messages-feed">
        {messages.map((message, index) => {
          const currMessageCreationDate = new Date(message.createdAt);
            // add zeros to form a unique identifier
            if(currMessageCreationDate > messageCreationDate) { 
              return (
                <Message 
                  message={message} 
                  activeUser={activeUser} 
                  key={'0000' + index}
                  onDeleteMessage={onDeleteMessage}
                  showModal={showModal}
                  hideModal={hideModal}
                  onEditMessage={onEditMessage}
                  onCancelEdition={onCancelEdition}
                  isModalOpen={isModalOpen}
                />)
            } else {
              return (
                <React.Fragment key={'000000' + index}>
                <DayLabel createdAt={message.createdAt} key={index}/>
                <Message 
                  message={message} 
                  activeUser={activeUser} 
                  key={'0000' + index}
                  onDeleteMessage={onDeleteMessage}
                  showModal={showModal}
                  hideModal={hideModal}
                  onEditMessage={onEditMessage}
                  onCancelEdition={onCancelEdition}
                  isModalOpen={isModalOpen}
                />
                </React.Fragment>
              )
            }
          }
        )}
      </div>
  );
}

export default MessageList;