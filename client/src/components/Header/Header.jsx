// eslint-disable-next-line
import React, { useState, useEffect } from 'react';
// eslint-disable-next-line
import { messagesURL, messagesList } from '../../config';
import './Header.css';
// eslint-disable-next-line
import LoadingScreen from '../LoadingScreen/LoadingScreen.jsx';



function Header({
    chatName,
    numOfParticipants,
    numOfMessages,
    lastMessageTime
}) {
    // const [isLoading, setIsLoading] = useState(true);

  return (
      <div id="header">
        <div className="header__tile">{chatName}</div>
        <div className="header__tile">{numOfParticipants} participants</div>
        <div className="header__tile">{numOfMessages} messages</div>
        <div className="header__tile header__tile-last">last message at {lastMessageTime}</div>
      </div>
  );
}

export default Header;