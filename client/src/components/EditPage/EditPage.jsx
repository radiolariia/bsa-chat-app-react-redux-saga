import React, { useState, useRef } from 'react';
import './EditPage.css';

function EditPage({
  onEditMessage,
  onCancelEdition,
  hideModal,
  message
}) {
  const [typedMessage, setTypedMessage] = useState('');

  const writeMessage = (text) => {
    setTypedMessage(text);
  }

  const EditMessage = (text) => {
    if(!text) {
      onCancelEdition();
    } else {
      const updatedMessage = {
        ...message,
        text: text,
        editedAt: new Date().toUTCString(),
      };

      onEditMessage(message.id, updatedMessage);
    }
    
    textareaElem.current.value = '';
    hideModal();
  }

  const textareaElem = useRef();
  return (
        <div id="blackout" onClick={() => hideModal()}>
            <div id="edit-window" onClick={(e) => e.stopPropagation()}>
              <span id="action-name">Edit Message</span>
                <div id="edit-input__container">
                    <textarea 
                        ref={textareaElem}
                        id="edit-input" 
                        placeholder="Edit message" 
                        name="Message" 
                        cols="40" 
                        rows="10"
                        onChange={() => writeMessage(textareaElem.current.value)}
                    ></textarea>
                </div>
                <div>
                  <button
                      className="edit-page__btn edit-input__send"
                      type="submit" 
                      onClick={() => EditMessage(typedMessage)}>
                      Apply changes
                  </button>
                  <button
                      className="edit-page__btn edit-input__cancel">
                      Cancel
                  </button>
                </div>
            </div>
        </div>
    );
}

export default EditPage;