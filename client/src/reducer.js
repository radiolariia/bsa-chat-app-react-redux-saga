import { combineReducers } from 'redux';

import { chatReducer } from './components/Chat/reducer';
import { loginReducer } from './components/LoginPage/reducer';

export const rootReducer = combineReducers({
  chatReducer,
  loginReducer
});
