import { 
    createStore, 
    applyMiddleware,
    compose 
} from 'redux';
import createSagaMiddleware from 'redux-saga';
import { rootSaga } from './rootSaga';
import { rootReducer } from './reducer';
import { FETCH_MESSAGES_SUCCESS } from './components/Chat/actionTypes'

const sagaMiddleware = createSagaMiddleware();

const composedEnhancers = compose(
    applyMiddleware(sagaMiddleware),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
   
);

export const store = createStore(
    rootReducer,
    composedEnhancers
);

sagaMiddleware.run(rootSaga);

  