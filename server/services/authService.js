import db from'../data/db.json';

class AuthService {
  login = ({ username, password }) => {
    try {
      const user = db.users.find(user => user.user === username);
    
    if(!user) {
      throw new Error('User with such username does NOT EXIST!');
    } else if(user.password !== password) {
      throw new Error('Password is incorrect!');
    } else {
      return user;
    }
    } catch(error) {
      return error.message;
    }
  };
}

export const authService = new AuthService();