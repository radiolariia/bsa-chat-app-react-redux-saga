import express from 'express';
import cors from 'cors';
import routes from './routes/index';

const app = express();

app.use(express.json());
app.use(cors());

routes(app);

app.use('/', express.static('./client/build'));

const port = 3090;
app.listen(port, () => {
    console.log(`server listening on ${port}`);
});

export default app;