import { Router } from 'express';

const router = Router();

router
  .put('/message', (req, res, next) => req.user
    .then(data => res.send(data))
    .catch(next))
  .post('/user', (req, res, next) => req.user
    .then(data => res.send(data))
    .catch(next));

    export default router;