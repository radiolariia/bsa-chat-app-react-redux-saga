import { Router } from 'express';

import { messageService } from '../services/messageService';

const router = Router();

router
  .get('/messages', (req, res, next) => {
      console.log("works")
      messageService.getAllMessages(req.user)
    })
    .then(data => res.send(data))
    .catch(next);

    export default router;