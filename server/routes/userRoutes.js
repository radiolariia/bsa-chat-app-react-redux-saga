import { Router } from 'express';

const router = Router();

router
  .get('/users', (req, res, next) => req.user
    .then(data => res.send(data))
    .catch(next));

    export default router;