import { Router } from 'express';
// const authenticationMiddleware = require('../middlewares/authenticationMiddleware');
import { authService } from '../services/authService';

const router = Router();

router
  .post('/', (req, res, next) => authService.login(req.user)
    .then(data => res.send(data))
    .catch(next));

    export default router;