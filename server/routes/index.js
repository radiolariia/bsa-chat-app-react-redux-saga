import { authRoutes } from  './authRoutes';
import { chatRoutes } from  './chatRoutes';
import { editorRoutes } from  './editorRoutes';
import { userRoutes } from  './userRoutes';
import { apiRoutes } from './apiRoutes';

export default app => {
  app.use('/api/auth', authRoutes);
  app.use('/api/chat', chatRoutes);
  app.use('/api/editor', editorRoutes);
  app.use('/api/users', userRoutes);
};
